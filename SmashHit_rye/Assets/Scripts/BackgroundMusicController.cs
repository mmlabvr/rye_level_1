﻿using UnityEngine;
using System.Collections;
using Rye;

public class BackgroundMusicController : MonoBehaviour
{
	private AudioSource m_backgroundMusic;
	private int m_currentBackgroundMusic;

	void Start ()
	{
		m_backgroundMusic = GetComponents<AudioSource>()[0];
		m_currentBackgroundMusic = 0;
		PlayNextBackground();
	}

	void Update ()
	{
		switch(GetComponent<GameController>().m_gameState)
		{
			case GameState.Playing:
				//if (!m_backgroundMusic.isPlaying)
				//{
				//	if (m_currentBackgroundMusic < Configs.s_maxBackgroundSfx)
				//		m_currentBackgroundMusic++;
				//	else
				//		m_currentBackgroundMusic = 1;

				//	m_backgroundMusic.clip = (AudioClip)Resources.Load(Configs.s_sfxBackground + m_currentBackgroundMusic);
				//	m_backgroundMusic.Play();
				//}

				break;
			case GameState.Pausing:
				m_backgroundMusic.Pause();

				break;
			case GameState.Failed:
				m_backgroundMusic.Stop();

				break;
		}
	}

	public void PlayNextBackground()
	{
		if (m_currentBackgroundMusic < Configs.s_maxBackgroundSfx)
			m_currentBackgroundMusic++;
		else
			m_currentBackgroundMusic = 1;

		m_backgroundMusic.clip = (AudioClip)Resources.Load(Configs.s_sfxBackground + m_currentBackgroundMusic);
		m_backgroundMusic.Play();
	}
}
