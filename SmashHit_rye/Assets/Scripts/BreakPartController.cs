﻿using UnityEngine;
using System.Collections;
using Rye;

public class BreakPartController : MonoBehaviour
{
	private float m_collisionMagnitude;
	private float m_collisionRadius;

	void Start ()
	{
		m_collisionMagnitude = 5.0f;
		m_collisionRadius = 0.5f;
	}
	
	void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.tag == "Marble")
		{
			if (col.relativeVelocity.magnitude > m_collisionMagnitude)
			{
				Collider[] colliders = Physics.OverlapSphere(col.transform.position, m_collisionRadius);
				foreach (Collider hit in colliders)
				{
					if (hit.GetComponent<Rigidbody>() && hit.tag == "Obstacle")
					{
						hit.GetComponent<Rigidbody>().isKinematic = false;
						hit.GetComponent<Rigidbody>().AddExplosionForce(Configs.s_explosionPower, col.transform.position, 0.0f);
					}
				}

				transform.parent.GetComponent<AudioSource>().Play();
			}
		}
	}
}
