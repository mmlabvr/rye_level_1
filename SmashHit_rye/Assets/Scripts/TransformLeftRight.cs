﻿using UnityEngine;
using System.Collections;
using Rye;

public class TransformLeftRight : MonoBehaviour
{
	public float m_halfDistance;
	public float m_speed;
	public Direction m_currentDirection;

	void Update ()
	{
		if(m_currentDirection == Direction.Left)
		{
			if (Camera.main.transform.position.x - transform.position.x < m_halfDistance)
				transform.Translate(-m_speed * Time.deltaTime, 0, 0);
			else
				StartCoroutine(ChangeDirectionAfter(Direction.Right, 1.0f));
		}
		else if(m_currentDirection == Direction.Right)
		{
			if (Camera.main.transform.position.x - transform.position.x > -m_halfDistance)
				transform.Translate(m_speed * Time.deltaTime, 0, 0);
			else
				StartCoroutine(ChangeDirectionAfter(Direction.Left, 1.0f));
		}
	}

	IEnumerator ChangeDirectionAfter(Direction dir, float seconds)
	{
		yield return new WaitForSeconds(seconds);

		m_currentDirection = dir;
	}
}
