﻿using UnityEngine;
using System.Collections;

public class HorizontalBarController : MonoBehaviour
{

	public GameObject m_original;
	public GameObject m_breakPart;

	void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.tag == "Marble")
		{
			m_original.SetActive(false);
			m_breakPart.SetActive(true);

			//Collider[] lstBreak = Physics.OverlapSphere(transform.position, 0.5f);
			//foreach (Collider collider in lstBreak)
			//{
			//	if (collider.GetComponent<Rigidbody>())
			//	{
			//		collider.gameObject.GetComponent<MeshRenderer>().enabled = true;
			//		collider.gameObject.GetComponent<Rigidbody>().isKinematic = false;
			//		collider.gameObject.GetComponent<Rigidbody>().AddExplosionForce(70.0f, collider.transform.position, 1.0f);
			//	}
			//}
			for (int i = 0; i < m_breakPart.transform.childCount; i++)
			{
				m_breakPart.transform.GetChild(i).GetComponent<Rigidbody>().isKinematic = false;
				m_breakPart.transform.GetChild(i).GetComponent<Rigidbody>().AddExplosionForce(100.0f, col.transform.position, 0.0f);
			}

			// TODO: Add sound break glass
		}
	}
}