﻿using UnityEngine;
using System.Collections;

public class GateController : MonoBehaviour
{
	////////////////////////////////////////////
	// Door info
	public	Transform m_door;
	private Transform m_doorLeft;
	private Transform m_doorRight;
	public Collider m_doorCollider;

	// Button info
	public	Transform m_buttonNormal;
	private Transform m_buttonLeft;
	private Transform m_buttonRight;
	private Transform m_button;

	public GameObject m_buttonPress;

	private bool m_isOpenDoor;
	private Vector3 m_doorLeftOpenPosition;
	private Vector3 m_doorRightOpenPosition;
	////////////////////////////////////////////


	bool flagChangeBackground;


	void Start()
	{
		m_isOpenDoor = false;

		m_doorLeft = m_door.GetChild(0);
		m_doorRight = m_door.GetChild(1);

		m_buttonLeft = m_buttonNormal.GetChild(0);
		m_button = m_buttonNormal.GetChild(1);
		m_buttonRight = m_buttonNormal.GetChild(2);

		m_doorLeftOpenPosition = new Vector3(m_door.position.x - 1.0f, m_door.position.y, m_door.position.z);
		m_doorRightOpenPosition = new Vector3(m_door.position.x + 1.0f, m_door.position.y, m_door.position.z);

		flagChangeBackground = false;
	}
	////////////////////////////////////////////



	void Update ()
	{
		if (m_isOpenDoor)
		{
			DoOpenDoor();
		}
		else
			DoButtonAttention();

		if (Camera.main.transform.position.z > gameObject.transform.position.z && !flagChangeBackground)
		{
			GameObject.FindGameObjectWithTag("GameController").GetComponent<BackgroundMusicController>().PlayNextBackground();
			flagChangeBackground = true;
		}
	}
	////////////////////////////////////////////



	// Set true for open door
	void OpenDoor()
	{
		m_isOpenDoor = true;
		m_doorCollider.enabled = false;

		m_button.GetComponent<AudioSource>().Play();
		m_door.GetComponent<AudioSource>().Play();
	}

	void DoOpenDoor()
	{
		m_doorLeft.position = Vector3.MoveTowards(m_doorLeft.position, m_doorLeftOpenPosition, Time.deltaTime);
		m_doorRight.position = Vector3.MoveTowards(m_doorRight.position, m_doorRightOpenPosition, Time.deltaTime);

		m_buttonPress.SetActive(true);
		m_buttonLeft.gameObject.SetActive(false);
		m_buttonRight.gameObject.SetActive(false);
	}
	////////////////////////////////////////////



	void DoButtonAttention()
	{
		m_buttonLeft.position	= new Vector3(m_button.position.x + (Mathf.PingPong(Time.time * 2f, 0.5f) - 1.2f), m_buttonLeft.position.y, m_buttonLeft.position.z);
		m_buttonRight.position	= new Vector3(m_button.position.x - (Mathf.PingPong(Time.time * 2f, 0.5f) - 1.2f), m_buttonRight.position.y, m_buttonRight.position.z);
	}
	////////////////////////////////////////////

	void OnCollisionEnter(Collision col)
	{
		if (!m_isOpenDoor)
		{
			OpenDoor();
		}
	}
}