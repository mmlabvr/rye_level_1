﻿using UnityEngine;
using System.Collections;
using Rye;

public class WallCollisionDetection : MonoBehaviour
{
	void OnCollisionEnter(Collision col)
	{
		if(col.gameObject.tag == "Marble")
		{
			GetComponent<AudioSource>().clip = (AudioClip)Resources.Load(Configs.s_sfxWallCollision + Random.Range(1, 5));
			GetComponent<AudioSource>().Play();
		}
	}

	void OnCollisionStay(Collision col)
	{
		if (col.gameObject.tag == "Marble")
		{
			if (col.gameObject.GetComponent<Rigidbody>().velocity.magnitude > 15.0f)
			{
				if (!GetComponent<AudioSource>().isPlaying)
				{
					GetComponent<AudioSource>().clip = (AudioClip)Resources.Load(Configs.s_sfxFloorCollision + Random.Range(1, 5));
					GetComponent<AudioSource>().Play();
				}
			}
		}
	}
}