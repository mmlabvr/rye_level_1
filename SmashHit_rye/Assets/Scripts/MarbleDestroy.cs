﻿using UnityEngine;
using System.Collections;

public class MarbleDestroy : MonoBehaviour
{
    Transform m_destroyer;

    void Start()
    {
        m_destroyer = GameObject.FindGameObjectWithTag("Destroyer").transform;
    }

	void Update ()
    {
        if (m_destroyer.position.z > transform.position.z)
            Destroy(gameObject);
	}
}