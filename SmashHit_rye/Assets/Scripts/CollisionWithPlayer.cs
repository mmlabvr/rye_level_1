﻿using UnityEngine;
using System.Collections;
using Rye;

public class CollisionWithPlayer : MonoBehaviour
{
	private Object m_marblePrefab;

	void Start()
	{
		m_marblePrefab = Resources.Load("Prefabs/pref_marble");
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "GameController")
		{
			if (GetComponent<Rigidbody>().isKinematic == true)
			{
				Statics.s_combo = 0;
				if (Statics.s_marbleLeft > 10)
					Statics.s_marbleLeft -= 10;
				else
				{
					Statics.s_marbleLeft = 0;
				}

				for (int i = 0; i < 10; i++)
				{
					GameObject marble = Instantiate(m_marblePrefab) as GameObject;
					marble.transform.position = col.gameObject.transform.position + new Vector3(0, 1.0f, 0);
					marble.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-1000, 1000), Random.Range(-5000, -3000), Random.Range(5000, 12000)));
				}

				Camera.main.GetComponent<CameraShake>().Shake();
				Camera.main.GetComponent<AudioSource>().Play();
			}
		}
	}
}