﻿using UnityEngine;
using System.Collections;
using Rye;

public class TransformUpDown : MonoBehaviour
{
	public float m_speed;
	public float m_waitingTime;

	public Direction m_currentDirection;
	public float m_top;
	public float m_bottom;	

	void Update()
	{
		if (m_currentDirection == Direction.Up)
		{
			if (transform.position.y < m_top)
				transform.Translate(0, m_speed * Time.deltaTime, 0);
			else
				StartCoroutine(ChangeDirectionAfter(Direction.Down, m_waitingTime));
		}
		else if (m_currentDirection == Direction.Down)
		{
			if (transform.position.y > m_bottom)
				transform.Translate(0, -m_speed * Time.deltaTime, 0);
			else
				StartCoroutine(ChangeDirectionAfter(Direction.Up, m_waitingTime));
		}
	}

	IEnumerator ChangeDirectionAfter(Direction dir, float seconds)
	{
		yield return new WaitForSeconds(seconds);

		m_currentDirection = dir;
	}
}
