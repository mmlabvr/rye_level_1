﻿using UnityEngine;
using System.Collections;
using Rye;

public class ObstaclesController : MonoBehaviour
{
	public GameObject m_brokenObject;
	public BreakStyle m_style;

	private AudioSource m_breakingSfx;

	private float m_collisionMagnitude;
	private float m_collisionRadius;

	void Start ()
	{
		m_collisionMagnitude = 5.0f;
		m_collisionRadius = 0.5f;
	}

	void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.tag == "Marble")
		{
			if (col.relativeVelocity.magnitude > m_collisionMagnitude)
			{
				GameObject brokenObject = Instantiate(m_brokenObject, transform.position, transform.rotation) as GameObject;
				brokenObject.transform.localScale = transform.localScale;

				if(gameObject.name == "Helix")
				{
					brokenObject.GetComponent<Rotate>().m_angle = gameObject.GetComponent<Rotate>().m_angle;
				}

				Destroy(gameObject);

				Collider[] colliders = Physics.OverlapSphere(col.transform.position, m_collisionRadius);
				foreach (Collider hit in colliders)
				{
					if (hit.GetComponent<Rigidbody>() && hit.tag == "Obstacle")
					{
						hit.GetComponent<Rigidbody>().isKinematic = false;
						hit.GetComponent<Rigidbody>().AddExplosionForce(Configs.s_explosionPower, col.transform.position, 0.0f);
					}
				}

				m_breakingSfx = brokenObject.GetComponent<AudioSource>();
				
				switch(m_style)
				{
					case BreakStyle.Light:
						m_breakingSfx.clip = (AudioClip)Resources.Load(Configs.s_sfxBreakingLight + Random.Range(1, 5));
						break;
					case BreakStyle.Medium:
						m_breakingSfx.clip = (AudioClip)Resources.Load(Configs.s_sfxBreakingMedium + Random.Range(1, 5));
						break;
					case BreakStyle.Heavy:
						m_breakingSfx.clip = (AudioClip)Resources.Load(Configs.s_sfxBreakingHeavy + Random.Range(1, 5));
						break;
					case BreakStyle.Panel:
						m_breakingSfx.clip = (AudioClip)Resources.Load(Configs.s_sfxBreakingPanel + Random.Range(1, 5));
						break;
				}
				m_breakingSfx.Play();

				Transform a;
			}
		}
	}
}