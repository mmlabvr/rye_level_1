﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour
{
	public float m_speed;
	public Vector3 m_angle;
	
	void Update ()
	{
		transform.Rotate(m_angle);
	}

	void OnCollisionEnter(Collision col)
	{
		if(col.gameObject.tag == "Marble")
		{

		}
	}
}