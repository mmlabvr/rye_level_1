﻿using UnityEngine;
using System.Collections;

namespace Rye
{
    public static class Statics
    {
        public static float s_cameraSpeed = 4.0f;
        public static bool s_isRapidShooting = false;

		public static int s_marbleLeft = 25;
		public static int s_combo = 0;
    }

	public static class Configs
	{
		public static readonly string s_sfxBackground = "SFX/sfx_background_";
		public static readonly int s_maxBackgroundSfx = 43;

		public static readonly string s_sfxShooting = "SFX/sfx_marble_shooting_";
		public static readonly string s_sfxBreakingLight = "SFX/sfx_breaking_glass_light_";
		public static readonly string s_sfxBreakingMedium = "SFX/sfx_breaking_glass_medium_";
		public static readonly string s_sfxBreakingHeavy = "SFX/sfx_breaking_glass_heavy_";
		public static readonly string s_sfxBreakingPanel = "SFX/sfx_breaking_glass_panel_";

		public static readonly string s_sfxWallCollision = "SFX/sfx_wall_collision_";
		public static readonly string s_sfxFloorCollision = "SFX/sfx_collision_with_floor_";

		public static readonly string s_sfxComboIncrease = "SFX/sfx_combo_increase";
		public static readonly string s_sfxComboReset = "SFX/sfx_combo_reset";

		public static readonly string s_sfxCrash = "SFX/sfx_crash";
		public static readonly string s_sfxFailed = "SFX/sfx_failed";

		public static readonly string s_sprComboPath = "Textures/spr_combo_";

		public static readonly int s_maxCombo = 49;
		public static readonly Vector3 s_ballSpeed = new Vector3(12000, 13000, 10000);
		public static readonly float s_explosionPower = 200.0f;

		public static readonly float s_marbleDistance = 0.2f;
	}

	public enum GameState
	{
		Playing,
		Pausing,
		Failed
	}

	public enum Direction
	{
		Left,
		Right,
		Up,
		Down,
		None
	}

	public enum BreakStyle
	{
		Light,
		Medium,
		Heavy,
		Panel
	}
}