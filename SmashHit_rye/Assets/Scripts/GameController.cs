﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Rye;

public class GameController : MonoBehaviour
{
	[HideInInspector]
	public GameState m_gameState;

    public GameObject m_marblePrefab;
    private float m_startTime;

	private AudioSource m_shootingSfx;
	private AudioSource m_failSfx;

	private bool flag;

	void Start ()
    {
		QualitySettings.vSyncCount = 1;
		//Application.targetFrameRate = 60;

        m_startTime = Time.time;

		m_shootingSfx = GetComponents<AudioSource>()[1];
		m_failSfx = GetComponents<AudioSource>()[2];

		flag = false;
		m_gameState = GameState.Playing;
	}
	
	void Update ()
    {
		switch(m_gameState)
		{
			case GameState.Playing:
				 transform.Translate(new Vector3(0, 0, Statics.s_cameraSpeed * Time.deltaTime));
				
				if (!Statics.s_isRapidShooting)
				{
					if (Input.GetMouseButtonDown(0) && Statics.s_marbleLeft > 0)
						Shoot();
				}
				else
				{
					if (Input.GetMouseButton(0) && Time.time - m_startTime >= 0.15f)
					{
						Shoot();
						m_startTime = Time.time;
					}
				}

				if (Statics.s_marbleLeft == 0)
				{
					m_gameState = GameState.Failed;

					m_failSfx.clip = (AudioClip)Resources.Load(Configs.s_sfxFailed);
					m_failSfx.Play();
				}

				break;
			case GameState.Pausing:

				break;
			case GameState.Failed:
				transform.Translate(new Vector3(0, 0, Statics.s_cameraSpeed * Time.deltaTime));
				if(Statics.s_cameraSpeed > 0)
					Statics.s_cameraSpeed -= 0.05f;
				else if (Statics.s_cameraSpeed < 0)
					Statics.s_cameraSpeed = 0;

				break;
			default:
				break;
		}
	}

    void Shoot()
    {
		CreateMarble();		
		Statics.s_marbleLeft--;
    }

	void CreateMarble()
	{
		Ray shootingDirection = Camera.main.ScreenPointToRay(Input.mousePosition);

		Vector3 marbleVelocity;
		marbleVelocity.x = Configs.s_ballSpeed.x * shootingDirection.direction.x;
		marbleVelocity.y = Configs.s_ballSpeed.y * (shootingDirection.direction.y + 0.1f);
		marbleVelocity.z = Configs.s_ballSpeed.z * shootingDirection.direction.z;

		int numberOfClone = Statics.s_combo / 10 + 1;

		if (numberOfClone == 1)
		{
			GameObject marble = Instantiate(m_marblePrefab);
			marble.transform.position = new Vector3(transform.position.x, transform.position.y + 0.25f, transform.position.z);
			marble.GetComponent<Rigidbody>().AddForce(marbleVelocity);
		}
		else if(numberOfClone == 2)
		{
			GameObject marble = Instantiate(m_marblePrefab);
			marble.transform.position = new Vector3(transform.position.x - Configs.s_marbleDistance, transform.position.y + 0.25f, transform.position.z);
			marble.GetComponent<Rigidbody>().AddForce(marbleVelocity);

			GameObject marble_1 = Instantiate(m_marblePrefab);
			marble_1.transform.position = new Vector3(transform.position.x + Configs.s_marbleDistance, transform.position.y + 0.25f, transform.position.z);
			marble_1.GetComponent<Rigidbody>().AddForce(marbleVelocity);
		}
		else if(numberOfClone == 3)
		{
			GameObject marble = Instantiate(m_marblePrefab);
			marble.transform.position = new Vector3(transform.position.x - Configs.s_marbleDistance, transform.position.y + 0.25f - Configs.s_marbleDistance, transform.position.z);
			marble.GetComponent<Rigidbody>().AddForce(marbleVelocity);

			GameObject marble_1 = Instantiate(m_marblePrefab);
			marble_1.transform.position = new Vector3(transform.position.x + Configs.s_marbleDistance, transform.position.y + 0.25f - Configs.s_marbleDistance, transform.position.z);
			marble_1.GetComponent<Rigidbody>().AddForce(marbleVelocity);

			GameObject marble_2 = Instantiate(m_marblePrefab);
			marble_2.transform.position = new Vector3(transform.position.x, transform.position.y + 0.25f + Configs.s_marbleDistance, transform.position.z);
			marble_2.GetComponent<Rigidbody>().AddForce(marbleVelocity);
		}
		else if(numberOfClone == 4)
		{
			GameObject marble = Instantiate(m_marblePrefab);
			marble.transform.position = new Vector3(transform.position.x - Configs.s_marbleDistance, transform.position.y + 0.25f - Configs.s_marbleDistance, transform.position.z);
			marble.GetComponent<Rigidbody>().AddForce(marbleVelocity);

			GameObject marble_1 = Instantiate(m_marblePrefab);
			marble_1.transform.position = new Vector3(transform.position.x - Configs.s_marbleDistance, transform.position.y + 0.25f + Configs.s_marbleDistance, transform.position.z);
			marble_1.GetComponent<Rigidbody>().AddForce(marbleVelocity);

			GameObject marble_2 = Instantiate(m_marblePrefab);
			marble_2.transform.position = new Vector3(transform.position.x + Configs.s_marbleDistance, transform.position.y + 0.25f - Configs.s_marbleDistance, transform.position.z);
			marble_2.GetComponent<Rigidbody>().AddForce(marbleVelocity);

			GameObject marble_3 = Instantiate(m_marblePrefab);
			marble_3.transform.position = new Vector3(transform.position.x + Configs.s_marbleDistance, transform.position.y + 0.25f + Configs.s_marbleDistance, transform.position.z);
			marble_3.GetComponent<Rigidbody>().AddForce(marbleVelocity);
		}
		else if(numberOfClone == 5)
		{
			GameObject marble = Instantiate(m_marblePrefab);
			marble.transform.position = new Vector3(transform.position.x, transform.position.y + 0.25f, transform.position.z);
			marble.GetComponent<Rigidbody>().AddForce(marbleVelocity);

			GameObject marble_1 = Instantiate(m_marblePrefab);
			marble_1.transform.position = new Vector3(transform.position.x - Configs.s_marbleDistance, transform.position.y + 0.25f - Configs.s_marbleDistance, transform.position.z);
			marble_1.GetComponent<Rigidbody>().AddForce(marbleVelocity);

			GameObject marble_2 = Instantiate(m_marblePrefab);
			marble_2.transform.position = new Vector3(transform.position.x - Configs.s_marbleDistance, transform.position.y + 0.25f + Configs.s_marbleDistance, transform.position.z);
			marble_2.GetComponent<Rigidbody>().AddForce(marbleVelocity);

			GameObject marble_3 = Instantiate(m_marblePrefab);
			marble_3.transform.position = new Vector3(transform.position.x + Configs.s_marbleDistance, transform.position.y + 0.25f - Configs.s_marbleDistance, transform.position.z);
			marble_3.GetComponent<Rigidbody>().AddForce(marbleVelocity);

			GameObject marble_4 = Instantiate(m_marblePrefab);
			marble_4.transform.position = new Vector3(transform.position.x + Configs.s_marbleDistance, transform.position.y + 0.25f + Configs.s_marbleDistance, transform.position.z);
			marble_4.GetComponent<Rigidbody>().AddForce(marbleVelocity);
		}

		m_shootingSfx.clip = (AudioClip)Resources.Load(Configs.s_sfxShooting + Random.Range(1, 4));
		m_shootingSfx.Play();
	}

	public void ShowPlus3()
	{
		GameObject text = GameObject.FindGameObjectWithTag("Plus");
		text.GetComponent<Text>().text = "+3";
		StartCoroutine(InActiveAfter(text, 1.0f));
	}

	IEnumerator InActiveAfter(GameObject target, float second)
	{
		yield return new WaitForSeconds(second);
		target.GetComponent<Text>().text = "";
	}
}