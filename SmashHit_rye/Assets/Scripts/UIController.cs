﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Rye;

public class UIController : MonoBehaviour
{
	public Text m_marbleLeft;
	public RawImage m_comboImage;

	void Update ()
	{
		m_marbleLeft.text = Statics.s_marbleLeft.ToString();

		m_comboImage.GetComponent<RawImage>().texture = (Texture)Resources.Load(Configs.s_sprComboPath + Statics.s_combo);
	}
}