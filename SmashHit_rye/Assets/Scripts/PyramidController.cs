﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Rye;

public class PyramidController : MonoBehaviour
{
	public GameObject m_brokenObject;
	public GameObject m_plusMarbles;
	public GameObject m_effect;

	private AudioSource m_comboSfx;
	private AudioSource m_breakingSfx;

	private float m_collisionMagnitude;
	private float m_collisionRadius;

	void Start()
	{
		m_collisionMagnitude = 5.0f;
		m_collisionRadius = 1.0f;

		m_comboSfx = GameObject.FindGameObjectWithTag("GameController").GetComponents<AudioSource>()[3];
	}

	void Update()
	{
		if (transform.position.z < Camera.main.transform.position.z)
		{
			if (Statics.s_combo > 9)
			{
				m_comboSfx.clip = (AudioClip)Resources.Load(Configs.s_sfxComboReset);
				m_comboSfx.Play();
			}
			Statics.s_combo = 0;

			Destroy(gameObject);
		}
	}

	void OnCollisionEnter(Collision col)
	{
		if(col.gameObject.tag == "Marble")
		{
			if(col.relativeVelocity.magnitude > m_collisionMagnitude)
			{
				Statics.s_marbleLeft += 3;
				if(Statics.s_combo < Configs.s_maxCombo)
					Statics.s_combo++;

				if(Statics.s_combo % 10 == 0)
				{
					m_comboSfx.clip = (AudioClip)Resources.Load(Configs.s_sfxComboIncrease);
					m_comboSfx.Play();
				}

				GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().ShowPlus3();

				Destroy(gameObject);

				GameObject brokenObject = Instantiate(m_brokenObject, transform.position, transform.rotation) as GameObject;
				brokenObject.transform.localScale = transform.localScale;

				m_breakingSfx = brokenObject.GetComponent<AudioSource>();
				m_breakingSfx.clip = (AudioClip)Resources.Load(Configs.s_sfxBreakingLight + Random.Range(1, 5));
				m_breakingSfx.Play();

				GameObject plusMarbles = Instantiate(m_plusMarbles);
				plusMarbles.transform.position = transform.position + new Vector3(0, 0.8f, 0);

				Instantiate(m_effect, transform.position, transform.rotation);

				Collider[] colliders = Physics.OverlapSphere(col.transform.position, m_collisionRadius);
				foreach(Collider hit in colliders)
				{
					if(hit.GetComponent<Rigidbody>())
					{
						hit.GetComponent<Rigidbody>().AddExplosionForce(Configs.s_explosionPower, col.transform.position, 0.0f);
					}
				}
			}
		}
	}
}